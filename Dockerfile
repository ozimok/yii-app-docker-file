FROM php:5.6-fpm

ENV YII_ENV Production
ENV PHP_EXTRA_CONFIGURE_ARGS --enable-fpm --with-fpm-user=www-data --with-fpm-group=www-data --enable-opcache

RUN apt-get update && apt-get install -y libmcrypt-dev libpq-dev \
    && docker-php-ext-install opcache iconv mcrypt pdo_pgsql pdo_mysql mbstring

RUN curl -L -o /tmp/redis.tar.gz https://github.com/phpredis/phpredis/archive/2.2.7.tar.gz \
    && tar xfz /tmp/redis.tar.gz \
    && rm -r /tmp/redis.tar.gz \
    && mv phpredis-2.2.7 /usr/src/php/ext/redis \
    && docker-php-ext-install redis

CMD /bin/bash -c "php /var/www/html/init --env=$YII_ENV --overwrite=y" && php-fpm